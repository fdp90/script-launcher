import requests


class Handler(object):
    def __init__(self, hostname, username, auth_token, project, repo, verbose=False):
        self.hostname = hostname
        self.username = username
        self.project = project
        self.repo = repo

        self.url = '{0}/rest/api/1.0/projects/{1}/repos/{2}/pull-requests'.format(self.hostname, self.project, self.repo)
        self.headers = {'Content-Type': 'application/json'}

        if auth_token:
            self.auth_token = auth_token
            self._set_auth(self.auth_token)

        self.verbose = verbose

    def _set_auth(self, token):
        self.headers['Authorization'] = "Bearer {0}".format(token)

    def _operation(self, operation, pr_id):
        return '{0}/{1}/{2}'.format(self.url, pr_id, operation)

    def post(self, uri, data, headers=None, verbose=False):
        headers = headers or self.headers
        response = requests.post(uri, data=data, headers=headers).json()

        if verbose or self.verbose:
            print(response)

        return response

    def get(self, uri, verbose=False):
        response = requests.get(uri)

        if verbose or self.verbose:
            print(response)

        return response

    def create(self, data, headers=None, verbose=False, **kwargs):
        return self.post(self.url, data=data, headers=headers, verbose=verbose)

    def comment(self, pr_id, data, verbose=False):
        return self.post(self._operation('comments', pr_id), data=data, verbose=verbose)

    def add_approver(self, pr_id, user, verbose=False):

        data = '''{{
    "user": {{
    }},
        "name": "{0}"
    "role": "REVIEWER"
}}
        '''.format(user)

        return self.post(self._operation('participants', pr_id), data=data, verbose=verbose)
