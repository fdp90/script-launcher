import unittest
from unittest.mock import patch
import pull_request_handler


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.pr_handler = pull_request_handler.Handler('http://localhost:7990', 'user', 'password', 'proj_key', 'repo')

    def test__operation(self):
        expected = 'http://localhost:7990/rest/api/1.0/projects/proj_key/repos/repo/pull-requests/21/comments'
        self.assertEqual(self.pr_handler._operation('comments', '21'), expected)

    @patch('pull_request_handler.print')
    @patch('requests.post')
    def test_post(self, mock_post, mock_print):
        expected = mock_post.return_value.json.return_value = {"key": "value"}
        result = self.pr_handler.post('URI', 'data', {"headerKey": "headerValue"})

        self.assertTrue(mock_post.called)
        self.assertEqual(result, expected)

        # Test with verbose
        self.pr_handler.post('URI', 'data', {"headerKey": "headerValue"}, verbose=True)
        self.assertTrue(mock_print.called)

    @patch('pull_request_handler.print')
    @patch('requests.get')
    def test_get(self, mock_get, mock_print):
        expected = mock_get.return_value = {"key": "value"}
        result = self.pr_handler.get('URI')

        self.assertEqual(result, expected)

        # Test with verbose
        self.pr_handler.get('URI', verbose=True)
        self.assertTrue(mock_print.called)

    @patch('pull_request_handler.Handler.post')
    def test_pull_request_create(self, mock_handler_post):
        mock_handler_post.return_value = {'key': 'value'}
        result = self.pr_handler.create(data="open('<filepath>', 'rb')")

        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, dict))

    @patch('pull_request_handler.Handler._operation')
    @patch('pull_request_handler.Handler.post')
    def test_pull_request_comment(self, mock_post, mock_operation):
        expected = mock_post.return_value = {'key': 'value'}

        result = self.pr_handler.comment(pr_id='31', data="open('<filepath>', 'rb')")

        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(result, expected)

        self.assertTrue(mock_post.called)
        self.assertTrue(mock_operation.called)


if __name__ == '__main__':
    unittest.main()
